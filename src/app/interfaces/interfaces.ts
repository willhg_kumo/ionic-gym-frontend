import { Component } from "@angular/core"

export interface Componente {
  icon: string;
  name: string;
  redirectTo: string;
}

export interface DatosPersonales {
  name: string;
  img: string;
}
