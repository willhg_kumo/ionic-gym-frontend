import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Componente } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  componentes: Observable<Componente[]>

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.componentes = this.dataService.getTabsOpt()
  }

}
