import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Componente, DatosPersonales } from '../interfaces/interfaces';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  componentes: string

  datosPersonales: DatosPersonales[] = [
    {
          name:'William',
          img : '../../../assets/William_500.jpg'
    }
  ]

  constructor(private http: HttpClient ) { }


  getMenuOpt() {
    return this.http.get<Componente[]>('/assets/data/menu.json')
  }

  getTabsOpt() {
    return this.http.get<Componente[]>('/assets/data/tabs.json')
  }
  getDatosPersonales() {
      return this.datosPersonales
  }

}
