import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';


import { MenuComponent } from './menu/menu.component';
import { DataService } from '../services/data.service';
import { HeaderComponent } from './header/header.component';



@NgModule({
  declarations: [
    MenuComponent,
    HeaderComponent


  ],
  exports: [
      MenuComponent,
      HeaderComponent
  ],
  providers: [DataService],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,


  ]
})
export class ComponentsModule { }
